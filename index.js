
const FIRST_NAME = "Diana-Iuliana";
const LAST_NAME = "Balan";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(a)
{
    
    var nr=parseInt(a);
    if(a<Number.MIN_VALUE || a>=Number.MAX_VALUE)
    {
        return NaN;
    }
    return nr;
    
}
function dynamicPropertyChecker(input, property) {
   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

